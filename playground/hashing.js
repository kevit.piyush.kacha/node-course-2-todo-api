const {SHA256}= require('crypto-js');

// let message ='I am user number 3';
// let hash = SHA256(message).toString();
//
// console.log(`Message: ${message}`);
// console.log(`Hash: ${hash}`);
//
// let data={
//     id:4
// };
//
// let token ={
//     data,
//     hash: SHA256(JSON.stringify(data)+'somesecret').toString()
// };
//
//
// token.data.id=4;
// token.hash= SHA256(JSON.stringify(data)).toString();
//
// let resultHash= SHA256(JSON.stringify(token.data)+'somesecret').toString();
//
// if(resultHash===token.hash){
//     console.log('Data was not change.');
// }
// else{
//     console.log('Data was change. Do not trust!!');
// }

const jwt = require('jsonwebtoken');

// let data={
//     id:10
// };
//
// let token = jwt.sign(data,'123abc');
//
// let decoded= jwt.verify(token,'123abc');
//
// console.log(decoded);

const bcrypt = require('bcryptjs');
let password='123abcss!';

bcrypt.genSalt(10,(err,salt)=>{

    bcrypt.hash(password,salt,(err,hash)=>{
        console.log(hash);
    });

});

let hashedPassword='$2a$10$c.r6RWlQuF4wIRgw4oAQTu1EwQyjxO1wAUMhqq/sn66nNOrCTKU6a';

bcrypt.compare(password,hashedPassword,(err,res)=>{

    console.log("Res",res);

});