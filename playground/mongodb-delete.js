//const MongoClient =require('mongodb').MongoClient;

// const {MongoClient,ObjectID} =require('mongodb');
// let obj= new ObjectID();
// console.log(obj);

const {MongoClient} =require('mongodb');

// let user={name:'piyush',age:25};
// let{age}=user;
// console.log(age);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err,db)=>{

    if(err){
      return  console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to Mongodb sever');

    // db.collection('Todos').deleteMany({text : "Eat Lunch"}).then(data=>{
    //
    //     console.log(data);
    // });

    // db.collection('Todos').deleteOne({text:"Eat"}).then(data=>{
    //
    //     console.log(data);
    // });

    // db.collection('Todos').findOneAndDelete({completed : true}).then(data=>{
    //     console.log(data);
    // });


    //User Collection Delete

    // db.collection('Users').deleteMany({name:"Pratik"}).then(data=>{
    //         console.log(data);
    // });


    db.collection('Users').findOneAndDelete({name:"Pratik"}).then(data=>{
        console.log(data);
    });

    //db.close();
});