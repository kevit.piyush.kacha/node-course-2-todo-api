//const MongoClient =require('mongodb').MongoClient;

// const {MongoClient,ObjectID} =require('mongodb');
// let obj= new ObjectID();
// console.log(obj);

const {MongoClient,ObjectID} =require('mongodb');

// let user={name:'piyush',age:25};
// let{age}=user;
// console.log(age);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err,db)=>{

    if(err){
      return  console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to Mongodb sever');

    // db.collection('Users').findOneAndUpdate({
    //  _id: new ObjectID('5b961f52a7ea6f132c2643b7')
    // },{
    //     $set:{
    //         name : "Piyush Kacha",
    //         age : 23,
    //         location : "Rajkot"
    //     }
    // },{
    //     returnOriginal:false
    // }).then(data=>{
    //     console.log("Data :",data);
    // });

    db.collection('Users').findOneAndUpdate({
        _id:new ObjectID('5b961f52a7ea6f132c2643b7')
    },{
        $set:{
            name:"Piyush Kacha",
        },$inc:{age: -5}
    },{
        returnOriginal:false
    }).then(data=>{
        console.log(data);
    });


    db.close();
});