let mongoose= require('mongoose');

let Todo = mongoose.model('Todo', {
    text: {
        type: String,
        required:true
    },
    completed: {
        type: Boolean,
        default:false
    },
    completedAt: {
        type: Number,
        default: null
    },
    _creator:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User',
        required: true
    }
});


module.exports = {Todo};


// let newTodo= new Todo({
//    text: 'Cook dinner',
//     completed: true,
//     completedAt: 10
// });
//
// newTodo.save().then(data=>{
//     console.log('Saved todo: ',data)
// },(err)=>{
//     console.log('Unable to save todo ')
// });


